var net = require('net')
const spawn = require('child_process').spawn
var server = new net.createServer()
server.listen(8080)
server.on("connection", (socket) => {
  console.log("connect " + socket.remoteAddress)
  var cwd
  socket.write('data>')
  socket.on('data', function(data) {
    var array = data.toString().replace(/[\n\r]/g, "").split(" ")
    var command = array[0]
    var args = array.slice(1)
    console.log(args)
    if(!command) {
      socket.write("no enter" + "\n")
      return
    }
    try {
      if (command == "cd") {
        cwd = args[0]
        return
      }
      spawn(command, args, { cwd: cwd }).on('error', (err) => {
        socket.write(err.message + "\n")
      })
      .stdout.pipe(socket, {end: false})
    } catch(error) {
      socket.write("not function \n")
    }
  })
})